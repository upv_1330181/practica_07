package course.examples.practica_07;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TabHost tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //Resources res = getResources();

        tabs = (TabHost) findViewById(R.id.tabHost); //llamamos al Tabhost
        tabs.setup();                                                         //lo activamos

        TabHost.TabSpec tab1 = tabs.newTabSpec("tab1");  //aspectos de cada Tab (pestaña)
        TabHost.TabSpec tab2 = tabs.newTabSpec("tab2");

        tab1.setIndicator("CALL");    //qué queremos que aparezca en las pestañas
        tab1.setContent(R.id.tab1); //definimos el id de cada Tab (pestaña)

        tab2.setIndicator("SMS");
        tab2.setContent(R.id.tab2);

        tabs.addTab(tab1); //añadimos los tabs ya programados
        tabs.addTab(tab2);

    }

    public void call(View view) {
        EditText texto = (EditText) findViewById(R.id.numero);
        String phone = texto.getText().toString();
        if(phone.isEmpty() || phone.length() < 10){
            Toast.makeText(getBaseContext(),"Favor de ingresar un numero correcto", Toast.LENGTH_LONG).show();
        }
        else{
            try {
                //834 227 15 59
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phone));
                startActivity(callIntent);
            } catch (ActivityNotFoundException activityException) {
                Log.e("dialing-example", "Call failed", activityException);
            }
        }
    }

    public void msj(View view) {
        EditText tell = (EditText) findViewById(R.id.numero2);
        EditText texto = (EditText) findViewById(R.id.txt_msj);

        String phoneNo = tell.getText().toString();
        String message = texto.getText().toString();

        if (phoneNo.isEmpty() || phoneNo.length() < 10) {
            Toast.makeText(getBaseContext(), "Favor de ingresar un numero correcto", Toast.LENGTH_LONG).show();
        } else {
                /*Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setData(Uri.parse("smsto:"));
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address"  , new String (phoneNo));
                smsIntent.putExtra("sms_body"  , new String (message));*/

                /*Uri uri = Uri.parse("smsto:"+phoneNo);
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("sms_body", new String(message));*/

            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                Toast.makeText(getApplicationContext(), "MSJ enviado.", Toast.LENGTH_LONG).show();
                //startActivity(smsIntent);
                //startActivity(it);
                //finish();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
